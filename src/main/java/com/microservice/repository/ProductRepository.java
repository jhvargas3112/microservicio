package com.microservice.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.microservice.model.Product;

public interface ProductRepository extends MongoRepository<Product, ObjectId> {

}
