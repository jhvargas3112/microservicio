package com.microservice.service.impl;

import java.util.List;
import java.util.NoSuchElementException;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import com.microservice.model.Product;
import com.microservice.repository.ProductRepository;
import com.microservice.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
  private ProductRepository productRepository;

  public ProductServiceImpl(ProductRepository productRepository) {
    this.productRepository = productRepository;
  }

  @Override
  public List<Product> findAll() {
    return productRepository.findAll();
  }

  @Override
  public Product findById(ObjectId id) throws NoSuchElementException {
    return productRepository.findById(id).get();
  }

  @Override
  public Product save(Product product) {
    return productRepository.save(product);
  }
}
