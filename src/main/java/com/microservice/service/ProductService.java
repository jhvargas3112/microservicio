package com.microservice.service;

import java.util.List;
import java.util.NoSuchElementException;
import org.bson.types.ObjectId;
import com.microservice.model.Product;

public interface ProductService {
  public List<Product> findAll();
  public Product findById(ObjectId id) throws NoSuchElementException;
  public Product save(Product product);
}
