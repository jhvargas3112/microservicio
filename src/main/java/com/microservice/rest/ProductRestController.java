package com.microservice.rest;

import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.microservice.model.Product;
import com.microservice.service.ProductService;

@RestController
@RequestMapping("/shop/products")
public class ProductRestController {
  private ProductService productService;

  public ProductRestController(ProductService productService) {
    this.productService = productService;
  }

  @GetMapping(value = "/")
  public List<Product> getAll() {
    return productService.findAll();
  }

  @GetMapping(value = "/{id}")
  public Product getById(@PathVariable(value = "id", required = true) ObjectId id) {
    return productService.findById(id);
  }

  @PostMapping(value = "/")
  @ResponseStatus(HttpStatus.CREATED)
  public Product create(@RequestBody Product product) {
    return productService.save(product);
  }
}
