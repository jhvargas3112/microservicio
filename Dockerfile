FROM openjdk:8-alpine

# Required for starting application up.
RUN apk update \
    && apk add bash

WORKDIR /opt/app

COPY ./target/springboot-shop-product-microservice-0.0.1-SNAPSHOT.jar .

CMD ["java", "-Dspring.data.mongodb.uri=mongodb://mongo:27017/shop","-Djava.security.egd=file:/dev/./urandom","-jar","./springboot-shop-product-microservice-0.0.1-SNAPSHOT.jar"]