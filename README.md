Microservicio para la cosulta y alta de productos.

INSTRUCCIONES PARA PROBAR EL MICROSERVICIO
---------------------------------------------

### CLONAR Y LEVANTAR EL MICROSERVICIO ###

1. Clonar el proyecto del repositorio de GitLab
	
	https://gitlab.com/jhvargas3112/microservicio.git

2. Desde la raíz del proyecto, generar el fichero .jar del microservicio con maven:
	
	mvn clean package

3. Desde la raíz del proyecto, construir y levantar la imagen del microservicio con Docker Compose:

	docker-compose build
	
	docker-compose up

### USAR EL MICROSERVICIO ###

1. Insertar un nuevo producto (POST):

	URL: localhost:8181/shop/products/
	
	BODY (ejemplo): {"name": "Coca-Cola", "price": 2.75}

2. Obtener todos los productos (GET):

    URL: localhost:8181/shop/products/

3. Obtener un producto por id (GET):
    
    URL: localhost:8181/shop/products/{id}